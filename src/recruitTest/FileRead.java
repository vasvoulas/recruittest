package recruitTest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class FileRead {

	public static void main(String[] args) {
		
		if (args.length!=3) {
			System.err.println("Please insert 3 arguments!");
			return;
		}
		
		try {
			
			FileInputStream fileStream1 = new FileInputStream(args[0]);
			BufferedReader buffReader1 = new BufferedReader(new InputStreamReader(fileStream1));
			String newLine = System.getProperty("line.separator");
			String strLine1, strLine2, strLine3;
			String tempFileName = "temp_"+args[2];
					
			File fDel = new File(args[2]);
			fDel.delete();
			
			//Loop first file
			while ((strLine1 = buffReader1.readLine()) != null)   {

				FileInputStream fileStream2 = new FileInputStream(args[1]);
				BufferedReader buffReader2 = new BufferedReader(new InputStreamReader(fileStream2));
				
				//Loop second file
				while ((strLine2 = buffReader2.readLine()) != null)   {
					if (strLine1.equals(strLine2)) {
						
						//Copy output file to temporary
						File f = new File(args[2]);
				    	File fTemp = new File(tempFileName);
				    	fTemp.delete();
				    	f.renameTo(fTemp);
				    	
				    	Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(args[2]), "UTF-8"));
				    	try {
				    		FileInputStream fileStream3 = new FileInputStream(tempFileName);
				    		BufferedReader buffReader3 = new BufferedReader(new InputStreamReader(fileStream3));

				    		//Loop temporary output file
							while ((strLine3 = buffReader3.readLine()) != null)   {
								if (strLine2.compareToIgnoreCase(strLine3)<0 && !"".equals(strLine2)){
									out.write(strLine2 + newLine);
									out.write(strLine3 + newLine);
									strLine2 = "";
								} else if (strLine2.equals(strLine3)) {
									strLine2 = "";
									out.write(strLine3 + newLine);
								} else {
									out.write(strLine3 + newLine);
								}
							}
							if (!"".equals(strLine2)) {
								out.write(strLine2 + newLine);
							}
							
							out.close();
							fileStream3.close();

						//Case first loop
				    	} catch (FileNotFoundException e) {
				    		out.write(strLine2 + newLine);
							out.close();
				    	}

					}
				}
				
				fileStream2.close();
				
			}
			
			fileStream1.close();
			
			//Delete temporary file
			File fTempDel = new File(tempFileName);
			fTempDel.delete();

		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
		
	}
	
}
