package recruitTest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class Utils {

	public static Set<String> file2set (String fileName) {

		Set<String> set = new HashSet<String>();

		try {
			
			FileInputStream fileStream = new FileInputStream(fileName);
			BufferedReader buffReader = new BufferedReader(new InputStreamReader(fileStream));
			String strLine;
			
			while ((strLine = buffReader.readLine()) != null)   {
				set.add(strLine);
			}
			
			fileStream.close();
			
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
		
		return set;
	}
	
	public static void list2file (List<String> outList, String fileName) {

		try {

			String newLine = System.getProperty("line.separator");
			Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8"));
			
			try {
		    	for (String temp : outList) {
		    		out.write(temp + newLine);
		    	}
			} finally {
			    out.close();
			}
			
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
		
	}

	public static List<String> set2orderedList (Set<String> setInput) {
		
		ArrayList<String> outList = new ArrayList<String>();
        outList.addAll(setInput);
        Collections.<String>sort(outList);
        
        return outList;
	}

}
